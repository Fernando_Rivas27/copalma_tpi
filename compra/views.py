from django.shortcuts import render,redirect
from django.urls import reverse
from .forms import CompraForm
from .models import Compra
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def compra(request):
    if request.method == "POST":
        form=CompraForm(data= request.POST)
        if form.is_valid():
          c=Compra()
          c.name=request.POST['nombre']
          c.email=request.POST['email']
          c.telefono=request.POST['telefono']
          c.fecha=request.POST['fecha']
          c.hora=request.POST['hora']
          c.quintal=request.POST['quintal']
          c.save()
        return  redirect(reverse('compra')+"?ok")
    else:
        form=CompraForm()
    return render(request, "compra/compra.html", {'form':form})

