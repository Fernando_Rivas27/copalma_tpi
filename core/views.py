from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.shortcuts import render, reverse, redirect
#django.contrib.auth.models.User
# Create your views here.
"""pagina estatica"""
@login_required
def home(request):
    return render(request, "core/home.html")
"""pagina estatica"""
@login_required
def about(request):
    return render(request, "core/about.html")


"""pagina estatica"""
@login_required
def store(request):
    return render(request, "core/store.html")
"""pagina estatica"""


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user is not None:    
          
              if user.is_superuser or user.is_staff:
                login(request, user)
                return redirect('/admin/')  # or your url name
              else:
                login(request, user)
                return redirect('/') 
         
    
        else:
           return redirect('login')
