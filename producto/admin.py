from django.contrib import admin
from .models import Producto
# Register your models here.

class ProductoAdmin(admin.ModelAdmin):
   readonly_fields=('fecha', 'hora')
   list_display=('nombre', 'telefono')
   ordering=('nombre',)
   search_fields=('nombre',)
   list_filter=('fecha','nombre', 'hora')

admin.site.register(Producto, ProductoAdmin)