from django.contrib import admin
from .models import Venta
# Register your models here.

class VentaAdmin(admin.ModelAdmin):
   readonly_fields=('fecha', 'hora')
   list_display=('nombre', 'telefono')
   ordering=('nombre',)
   search_fields=('nombre',)
   list_filter=('fecha','nombre', 'hora')

admin.site.register(Venta, VentaAdmin)