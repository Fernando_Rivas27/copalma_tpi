from django import forms

class CompraForm(forms.Form):
    nombre=forms.CharField(label="Nombre",required=True,widget=forms.TextInput(
        attrs={'class':'form-control', 'placeholder':'Escriba su nombre'}
    ))

    email=forms.EmailField(label="Email", required=True,widget=forms.EmailInput(
        attrs={'class':'form-control', 'placeholder':'Escriba su email'}
    ))

    telefono=forms.CharField(label="Telefono", required=True,widget=forms.TextInput(
        attrs={'class':'form-control', 'placeholder':'Escriba su telefono'}
    ))

    fecha=forms.DateField(label="Fecha", required=True,widget=forms.DateInput(
        attrs={'type':'date','class':'form-control'}
    ))
    
    hora=forms.TimeField(label="Hora", required=True,widget=forms.TimeInput(
        attrs={'type':'time','class':'form-control'}
    ))
    
    quintal=forms.CharField(label="Cantidad de Quintales", required=True,widget=forms.TextInput(
        attrs={'class':'form-control', 'placeholder':'Escriba la cantidad en quintales a vender', 'type':'number','min':'1'}
    ))