from django.shortcuts import render,redirect
from django.urls import reverse
from .forms import ProductoForm
from .models import Producto
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def producto(request):
    if request.method == "POST":
        form=ProductoForm(data= request.POST)
        if form.is_valid():
          c=Producto()
          c.nombre=request.POST['nombre']
          c.email=request.POST['email']
          c.telefono=request.POST['telefono']
          c.fecha=request.POST['fecha']
          c.hora=request.POST['hora']
          c.cantidad=request.POST['cantidad']
          c.save()
        return  redirect(reverse('producto')+"?ok")
    else:
        form=ProductoForm()
    return render(request, "producto/producto.html", {'form':form})