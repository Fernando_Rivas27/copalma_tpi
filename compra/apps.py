from django.apps import AppConfig


class CompraConfig(AppConfig):
    name = 'compra'
    verbose_name='Gestor de compra'
