from django.apps import AppConfig


class VentaConfig(AppConfig):
    name = 'venta'
    verbose_name='Gestor de venta'
