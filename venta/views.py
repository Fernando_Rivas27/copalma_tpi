from django.shortcuts import render,redirect
from django.urls import reverse
from .forms import VentaForm
from .models import Venta
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def venta(request):
    if request.method == "POST":
        form=VentaForm(data= request.POST)
        if form.is_valid():
          c=Venta()
          c.nombre=request.POST['nombre']
          c.email=request.POST['email']
          c.telefono=request.POST['telefono']
          c.cantidad=request.POST['cantidad']
          c.fecha=request.POST['fecha']
          c.hora=request.POST['hora']
          c.save()
        return  redirect(reverse('venta')+"?ok")
    else:
        form=VentaForm()
    return render(request, "venta/venta.html", {'form':form})

