from django.db import models

# Create your models here.

class Venta(models.Model):
    #atributos de la clase
    nombre =models.CharField(max_length=50)
    email=models.EmailField(max_length=50)
    telefono=models.CharField(max_length=50)
    cantidad=models.CharField(max_length=50)
    fecha=models.DateField()
    hora=models.TimeField()
    
    class Meta:
        verbose_name="venta"
        verbose_name_plural="ventas"
        ordering=['-nombre']

    #__str__() para devolver una cadena de texto legible por humanos para cada objeto.
    def __str__(self):
        return self.nombre # Con frecuencia éste devolverá un título o nombre de campo del modelo.